<?php

namespace Oxit\FlashMessages;


use Contributte\Events\Extra\Event\Application\PresenterEvent;
use Nette\Application\UI\Presenter;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class FlashMessageManager implements EventSubscriberInterface
{

	private const SEVERITY_ERROR = 'error';
	private const SEVERITY_WARNING = 'warning';
	private const SEVERITY_INFO = 'info';
	private const SEVERITY_SUCCESS = 'success';

	/** @var Presenter */
	private $presenter;

	public static function getSubscribedEvents(): array
	{
		return [PresenterEvent::class => 'setPresenter'];
	}

	/** @internal */
	public function setPresenter(PresenterEvent $event): void
	{
		/** @var Presenter $presenter */
		$presenter = $event->getPresenter();
		$this->presenter = $presenter;
	}

	public function error($message): void
	{
		$this->presenter->flashMessage($message, self::SEVERITY_ERROR);
		$this->presenter->redrawControl('flashes');
	}

	public function warning($message): void
	{
		$this->presenter->flashMessage($message, self::SEVERITY_WARNING);
		$this->presenter->redrawControl('flashes');
	}

	public function info($message): void
	{
		$this->presenter->flashMessage($message, self::SEVERITY_INFO);
		$this->presenter->redrawControl('flashes');
	}

	public function success($message): void
	{
		$this->presenter->flashMessage($message, self::SEVERITY_SUCCESS);
		$this->presenter->redrawControl('flashes');
	}

}
