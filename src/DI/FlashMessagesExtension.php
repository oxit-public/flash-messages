<?php

namespace Oxit\FlashMessages\DI;

use Nette\DI\CompilerExtension;
use Oxit\FlashMessages\FlashMessageManager;

class FlashMessagesExtension extends CompilerExtension
{

	public function loadConfiguration()
	{
		$builder = $this->getContainerBuilder();

		$builder->addDefinition($this->prefix('flashMessageManager'))
			->setType(FlashMessageManager::class)
			->addTag('kdyby.subscriber');
	}

}
